/**
 * Функция для вывода (точка входа)
 * @param elem - идентификатор элемента
 * @param url - адрес подфорума анкет (должен быть открыт для гостей)
 */
function renderCharlist(elem, url) {
    $(elem).html(processPages(url));
}

/**
 * Асинхронно получить содержимое страницы
 * @param url
 * @returns {string}
 */
function getRemote(url) {
    return $.ajax({
        type: "GET",
        url: url,
        async: false
    }).responseText;
}

/**
 * Отсортировать список фандомов и анкет и вывести в виде html
 * @param charSheet
 * @returns {string}
 */
function formList(charSheet) {
    var sortObj = sortObject(charSheet);
    var html = '<ul id="fandom-list">';
    $.each(sortObj, function (key, value) {
        html += '<li class="fandom-node"><span class="fandom-title">' + key + '</span><ul class="charseets-list">';
        $.each(value, function (key, obj) {
            var sortObj2 = obj.sort(function(a, b) {
                if(a.name < b.name) return -1;
                if(a.name > b.name) return 1;
                return 0;
            });
            $.each(sortObj2, function (key, tmp) {
                html += '<li class="charsheet-node"><a href=">' + tmp.link + '">' + tmp.name + '</a></li>';
            })
        })
        html += '</li></ul>';
    })
    html += '</ul>';
    return html;
}

/**
 * Обработка информации на страницах, формирование списка фандомов и анкет
 * @param url
 */
function processPages(url) {
    var links = {};
    var $html = $(getRemote(url));
            var lastPage = $html.find('.linkst a.next').attr('href');
        var pageLinks = getAllPages(lastPage);
        var tmp = getCharsheetsFromPage($html);
        $.each(tmp, function (index, value) {
            if (!links[index]) {
                links[index] = [];
            }
            links[index].push(value);
        });
            $.each(pageLinks, function (ind, url) {
                var $html = $(getRemote(url));
                var tmp = getCharsheetsFromPage($html);
                $.each(tmp, function (index, value) {
                    if (!links[index]) {
                        links[index] = [];
                    }
                    links[index].push(value);
                });
        });
    return formList(links);
}

/**
 * Формирование списка всех страниц подфорума
 * @param lastPageSrc
 * @returns {Array}
 */
function getAllPages(lastPageSrc) {
    var pages = [];
    var arr = lastPageSrc.split('&p=');
    for (var i = 2; i <= arr[1]; i++) {
        pages.push(arr[0] + '&p=' + i);
    }
    return pages;
}

/**
 * Сортировка объектов
 * @param o
 * @returns {{}}
 */
function sortObject(o) {
    var sorted = {},
        key, a = [];
    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }
    a.sort();
    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
}

/**
 * Распарсить одну страницу и вернуть список фандомов и анкет
 * @param $html
 * @returns {{}}
 */
function getCharsheetsFromPage($html) {
    var $links = $html.find('.tcl a');
    var links = {};
    $.each($links, function (index, value) {
        var tmp = dissampleTitle($(value).text());
        tmp.link = $(value).attr('href');
        if (!links[tmp.fandom]) {
            links[tmp.fandom] = [];
        }
        links[tmp.fandom].push(tmp);
    });
    return links;
}

/**
 * Распарсить название анкеты, получить имя персонажа и название фандома
 * @param str
 * @returns {{name: string, fandom: string}}
 */
function dissampleTitle(str) {
    str = $.trim(str);
    var arr = str.split('[');
    var name = $.trim(arr[0]);
    var fandom = $.trim(arr[1].substr(0, arr[1].length - 1));
    return {
        name: name,
        fandom: fandom
    }
}

