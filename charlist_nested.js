/**
 * Функция для вывода (точка входа)
 * @param elem - идентификатор элемента
 * @param url - адрес подфорума анкет (должен быть открыт для гостей)
 */
function renderCharlist(elem, url) {
    $(elem).html(processPages(url));
}

/**
 * Асинхронно получить содержимое страницы
 * @param url
 * @returns {string}
 */
function getRemote(url) {
    return $.ajax({
        type: "GET",
        url: url,
        async: false
    }).responseText;
}

/**
 * Отсортировать список фандомов и анкет и вывести в виде html
 * @param charSheet
 * @returns {string}
 */
function formList(charSheet) {
    var sortMainFandoms = sortObject(charSheet);
    var html = '<ul class="fandom-list fandom-level-0 main-fandom-list">';
    $.each(sortMainFandoms, function (key, value) {
        html += renderFandomsRecursive(key, value, 1);
    })
    html += '</ul>';
    return html;
}

/**
 * Нарисовать дерево фандомов и персонажей (рекурсивно)
 * @param title
 * @param node
 * @param number
 * @returns {*}
 */
function renderFandomsRecursive(title, node, number) {
    var html = null;
    var n = Object.keys(node).length;
    if (n == 0) {
        html = node;
    }
    node = sortObject(node);
    if (n == 1) {
        html = '<li class="fandom-node fandom-level-' + number + '"><span class="fandom-name fandom-level-' + number + '">' + title + '</span>';
        html += renderList(node.__charsheets);
        html += '</li>';
    }
    if (n > 1) {
        html = '<li class="fandom-node fandom-level-' + number + '"><span class="fandom-name fandom-level-' + number + '">' + title + '</span>';
        $.each(node, function (key, value) {
            if (key !== '__charsheets') {
                html += '<ul class="fandom-list fandom-level-' + number + '">';
                html += renderFandomsRecursive(key, value, (number + 1));
                html += '</ul>';
            } else {
                html += renderList(value);
            }
        })
        html += '</li>';
    }
    return html;
}

/**
 * Вернуть html-код ссылки на анкету
 * @param charsheets
 * @returns {string}
 */
function renderList(charsheets) {
    charsheets.sort(function (a, b) {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
    });
    var html = '<ul class="charlist">';
    $.each(charsheets, function (key, char) {
        html += '<li class="charsheet-node"><a href="' + char.link + '">' + char.name + '</a></li>';
    })
    html += '</ul>';
    return html;
}

/**
 * Обработка информации на страницах, формирование списка фандомов и анкет
 * @param url
 */
function processPages(url) {
    var $html = $(getRemote(url));
    var lastPage = $html.find('.linkst a.next').attr('href');
    if (lastPage === undefined) {
        var pageLinks = [];
    } else {
        var pageLinks = getAllPages(lastPage);
    }
    var links = {};
    var tmp = getCharsheetsFromPage($html);
    links = extendext(true, "concat", links, tmp);
    $.each(pageLinks, function (ind, url) {
        var $html = $(getRemote(url));
        var tmp = getCharsheetsFromPage($html);
        links = extendext(true, "concat", links, tmp);
    });
    return formList(links);
}

/**
 * Формирование списка всех страниц подфорума
 * @param lastPageSrc
 * @returns {Array}
 */
function getAllPages(lastPageSrc) {
    var pages = [];
    var arr = lastPageSrc.split('&p=');
    for (var i = 2; i <= arr[1]; i++) {
        pages.push(arr[0] + '&p=' + i);
    }
    return pages;
}

/**
 * Сортировка объектов
 * @param o
 * @returns {{}}
 */
function sortObject(o) {
    var sorted = {},
        key, a = [];
    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }
    a.sort();
    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
}

/**
 * Распарсить одну страницу и вернуть список фандомов и анкет
 * @param $html
 * @returns {{}}
 */
function getCharsheetsFromPage($html) {
    var $links = $html.find('.tcl a');
    links = {};
    $.each($links, function (index, value) {
        var tmp = dissampleTitle($(value).text());
        tmp.link = $(value).attr('href');
        var cur = links;
        for (var i = 0; i < tmp.fandom.length - 1; i++) {
            if (!cur[tmp.fandom[i]]) {
                cur[tmp.fandom[i]] = {__charsheets: []};
            }
            cur = cur[tmp.fandom[i]];
        }
        var tmpKey = tmp.fandom[tmp.fandom.length - 1];

        if (!cur[tmpKey]) {
            cur[tmpKey] = {__charsheets: []};
        }
        var arr = cur[tmpKey].__charsheets.push({name: tmp.name, link: tmp.link});
    });
    return links;
}

/**
 * Распарсить название анкеты, получить имя персонажа и названия фандомов
 * @param str
 * @returns {{name: string, fandom: string}}
 */
function dissampleTitle(str) {
    str = $.trim(str);
    var arr = str.split('[');
    var name = $.trim(arr[0]);
    var fandoms = $.trim(arr[1].substr(0, arr[1].length - 1));
    var fandomArr = fandoms.split('/');
    var fandomArrTrimmed = [];
    $.each(fandomArr, function (index, value) {
        fandomArrTrimmed.push($.trim(value));
    })
    return {
        name: name,
        fandom: fandomArrTrimmed
    }
}


/*!
 * jQuery.extendext 0.1.1
 *
 * Copyright 2014 Damien "Mistic" Sorel (http://www.strangeplanet.fr)
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 *
 * Based on jQuery.extend by jQuery Foundation, Inc. and other contributors
 */

/*jshint -W083 */
function extendext() {
    var options, name, src, copy, copyIsArray, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false,
        arrayMode = 'default';

    // Handle a deep copy situation
    if (typeof target === "boolean") {
        deep = target;

        // Skip the boolean and the target
        target = arguments[i++] || {};
    }

    // Handle array mode parameter
    if (typeof target === "string") {
        arrayMode = $([target.toLowerCase(), 'default']).filter(['default', 'concat', 'replace', 'extend'])[0];

        // Skip the string param
        target = arguments[i++] || {};
    }

    // Handle case when target is a string or something (possible in deep copy)
    if (typeof target !== "object" && !$.isFunction(target)) {
        target = {};
    }

    // Extend jQuery itself if only one argument is passed
    if (i === length) {
        target = this;
        i--;
    }

    for (; i < length; i++) {
        // Only deal with non-null/undefined values
        if ((options = arguments[i]) !== null) {
            // Special operations for arrays
            if ($.isArray(options) && arrayMode !== 'default') {
                clone = target && $.isArray(target) ? target : [];

                switch (arrayMode) {
                    case 'concat':
                        target = clone.concat($.extend(deep, [], options));
                        break;

                    case 'replace':
                        target = $.extend(deep, [], options);
                        break;

                    case 'extend':
                        options.forEach(function (e, i) {
                            if (typeof e === 'object') {
                                var type = $.isArray(e) ? [] : {};
                                clone[i] = $.extendext(deep, arrayMode, clone[i] || type, e);

                            } else if (clone.indexOf(e) === -1) {
                                clone.push(e);
                            }
                        });

                        target = clone;
                        break;
                }

            } else {
                // Extend the base object
                for (name in options) {
                    src = target[name];
                    copy = options[name];

                    // Prevent never-ending loop
                    if (target === copy) {
                        continue;
                    }

                    // Recurse if we're merging plain objects or arrays
                    if (deep && copy && ( $.isPlainObject(copy) ||
                        (copyIsArray = $.isArray(copy)) )) {

                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && $.isArray(src) ? src : [];

                        } else {
                            clone = src && $.isPlainObject(src) ? src : {};
                        }

                        // Never move original objects, clone them
                        target[name] = extendext(deep, arrayMode, clone, copy);

                        // Don't bring in undefined values
                    } else if (copy !== undefined) {
                        target[name] = copy;
                    }
                }
            }
        }
    }

    // Return the modified object
    return target;
};